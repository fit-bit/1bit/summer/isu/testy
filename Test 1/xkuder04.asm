%include "rw32-2018.inc"

section .data
    a db 5
    b db 7
    c db 2
    d dw 6
    e dw 10
    f dd 7
    v dd 0
    z dd 0

section .text
CMAIN:
    push ebp
    mov ebp,esp
    
        mov eax,0x44332211
    call task11
    
    call task12
    
    pop ebp
    ret
    
;
;--- Úkol 1 ---
;
; Naprogramujte funkci "task11", který zamění pořadí slabik (bytů) X1, X2, X3, X4 v registru EAX takto:
;
; původní hodnota:  EAX = X4 X3 X2 X1
; výsledná hodnota: EAX = X3 X1 X4 X2 
;
; Funkci je hodnota předána v resgistru EAX a výsledek vrací také v registru EAX. Nejnižší významová
; slabika (byte) je X1, nejvyšší významová slabika (byte) je X4.
;
; Vstup:
;    - EAX = 32bitová hodnota
;
; Výstup:
;    - EAX = výsledek
;    - funkce nemusí zachovat obsah registrů
;
task11:
    ; zde zadejte kód funkce task11
    ror eax, 8
    xchg al, ah
    ror eax, 8  
    ret

;;--- Úkol 2 ---
;
; Naprogramujte funkci "task12", který vypočítá následující výraz SE ZNAMÉNKEM:
;
; v = ((a - b)*c + d*e)/f ... celočíselný podíl
; z = ((a - b)*c + d*e)%f ... zbytek po dělení f
;
; Vstupní hodnoty jsou uloženy v paměti a jsou definovány takto:
;
;    [a] ...  8bitové číslo se znaménkem
;    [b] ...  8bitové číslo se znaménkem
;    [c] ...  8bitové číslo se znaménkem
;    [d] ... 16bitové číslo se znaménkem
;    [e] ... 16bitové číslo se znaménkem
;    [f] ... 32bitové číslo se znaménkem
;
; Výsledek uložte do paměti na adresu v a z takto:
;
;    [v] ... výsledek - vypočítaná hodnota výrazu (32bitové číslo se znaménkem)
;    [z] ... zbytek po dělení hodnotou f (32bitové číslo se znaménkem)
;
; Důležité:
;  - funkce nesmí měnit obsah registru ESI
;  - dělení nulou neberte v úvahu
;
; Kontrolní výpočet:
;    a = 5, b = 7, c = 2, d = 6, e = 10, f = 7
;    v = 8, z = 0
task12:
    ; zde zadejte kód funkce task12
    
    mov al, [a]
    mov ah, [b]
    sub al, ah ;AL =(a-b) = -2
    
    mov bl, [c]
    imul bl ; AX = (a - b)*c = -4
    mov cx, ax ; CX = (a - b)*c = -4
    
    xor eax, eax
    mov ax, [d]
    mov bx, [e]
    imul bx ; DX:AX = d*e = 60
    rol eax, 16
    mov ax, dx
    rol eax, 16 ; EAX = d*e = 60
    
    movsx ecx, cx ; ECX = (a - b)*c = -4
    add eax, ecx ; EAX = ((a - b)*c + d*e) = 56
    
    cdq ; EDX:EAX = ((a - b)*c + d*e) = 56
    mov ebx, [f]
    idiv ebx ; EAX = ((a - b)*c + d*e)/f = 8 EDX = ((a - b)*c + d*e)%f = 0
    mov [v], eax
    mov [z], edx
    
    ret
