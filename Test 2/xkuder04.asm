%include "rw32-2018.inc"

section .data
    task21A dd 3072,-256,2304,-2048,-1792,1536,-15360,-14336
    task21B dd 2027472952,1166214482,-1855339679,-1282100569,-321342753,-1713896608,-1485562887,244776901
    task22A dw 384,176,336,64,80,288,-768,-704
    task22B dw 27230,9965,1416,7839,57059,54969,6324,33054
    task23A dw 192,-16,144,-128,-112,96,-960,-896

section .text
CMAIN:
    push ebp
    mov ebp,esp
    
    mov eax,task21A
    mov ebx,task21B
    mov ecx,8
    call task21

    
    ; zde bude volání funkce: task22(task22A,8,0)
    ; musíte uložit parametry ve správném pořadí
    ; a uklidit zásobník, pokud je to třeba
    push task22A
    push 0
    push 100
    call task22    

    
    mov esi,task22A
    mov ecx,8
    call task23

    pop ebp
    ret    
;           
;--- Úkol 1 --- 
;
; Naprogramujte funkci, která zkopíruje pole 32bitových hodnot z pole, na které
; ukazuje registr EAX do pole, na který ukazuje registr EBX. Počet prvků obou polí je v registru ECX.
;
; Vstup:
;   EAX = ukazatel na pole 32bitových hodnot, které chci zkopírovat
;   EBX = ukazatel na místo, kam chci pole EAX zkopírovat
;   ECX = počet prvků polí (32bitová hodnota bez znaménka, vždy větší než 0)
;
; Výstup:
;   Kopie pole z adresy EAX na adresu EBX.
;
; Důležité:
;   Funkce NEmusí zachovat obsah registrů.
;
; Hodnocení: až 2 body.
;
task21:
    ; zde zadejte kód funkce task21
    push ebp
    mov ebp, esp
    
    ; ecx u� je nastaven� a nemus�me ho zachovat
    presun:
        mov edx, [eax+ecx*4-4]
        mov [ebx+ecx*4-4], edx
        loop presun
    
    mov esp, ebp
    pop ebp
    ret
;
;--- Úkol 2 ---
;
; Naprogramujte funkci, která v poli 16bitových čísel bez znaménka pA spočítá výskyt všech 
; hodnot menších než je hodnota x. Délka pole je dána parametrem N. 
;
; Funkci jsou předávány parametry na zásobníku v pořadí od prvního k poslednímu (zleva doprava), parametry ze 
; zásobníku uklízí volaný a výsledek funkce se vrací v registru EAX (konvence jazyka Pascal).
;
; int task22(const unsigned short *pA, int N, unsigned short x)
;
; Vstup:
;   pA: ukazatel na pole (pole obsahuje 16bitové hodnoty bez znaménka)
;    N: počet prvků pole (32bitové číslo se znaménkem)
;    x: hodnota, kterou hledáme (16bitové číslo bez znaménka)
;
; Výstup:
;   EAX = počet prvků pole, která splňují zadanou podmínku
;   EAX = -1 <=> neplatný ukazatel *pA (tj. pA == 0) nebo N < 0
;
; Důležité:
;   Funkce musí zachovat obsah všech registrů, kromě registru EAX a příznakového registru.
;
; Hodnocení: až 5 bodů.
;
task22:
    ; zde zadejte kód funkce task22
    push ebp
    mov ebp, esp
    
    xor eax, eax
    
    ; musim zachovat
    push ebx
    push ecx
    push edx
    
    ; *pA [ebp+16]
    ; N [ebp+12]
    ; x [ebp+8]
   
    ; odkaz na pole
    mov ebx, [ebp+16] 
    
    ; neplatn� ukazatel *pA (tj. pA == 0)
    cmp ebx, 0
    je bad_arguments
    
    ; N < 0
    cmp [ebp+12], dword 0
    jl bad_arguments
    
    ; projiti pole
    mov ecx, [ebp+12]
    mov edx, [ebp+8] ;x
    
    ; kontrola jestli velikost pole je 0
    cmp ecx, 0
    je do_nothing
    
    cyklus:
        cmp [ebx+ecx*2-2], dx
        jnb not_inc
        inc eax
       not_inc:
        loop cyklus
        
   do_nothing:
    jmp good
    
  bad_arguments:
    mov eax, -1
    
  good:
    
    pop edx
    pop ecx
    pop ebx
    
    mov esp, ebp
    pop ebp
    ret 12;4*n
;
;--- Úkol 3 ---
;
; Naprogramujte funkci, která vzestupně (od nejmenšího k největšímu) seřadí pole 16bitových prvků se znaménkem. 
;
; Ukázka algoritmu řazení v jazyce C:
;
; void task23(short *pA, unsigned int N) {
;   for(int i = 0; i < N; i++) {
;     for(int j = i + 1; j < N; j++) {
;       if (pA[i] > pA[j]) { tmp = pA[i]; pA[i] = pA[j]; pA[j] = tmp; }      
;     }
;   }
; }
;
; Vstup:
;   ESI: ukazatel na pole 16bitových hodnot se znaménkem
;   ECX: počet prvků pole (32bitové číslo bez znaménka)
;
; Výstup:
;   Na adrese ESI je pole seřazené dle velikosti.
;
; Důležité:
;   Funkce musí zachovat obsah všech registrů, kromě registru EAX a příznakového registru.
;
; Hodnocení: až 5 bodů.
;
task23:
    ; zde zadejte kód funkce task23
    push ebp
    mov ebp, esp
    
    ; zachovat
    push edx
    push ebx
    
  ; vnejsi cyklus
  mov ebx, 0
  for1:
    cmp ebx, ecx
    je for1_end
    
    ; vnitrni cyklus
    mov edx, ebx
    inc edx
   for2:
    cmp edx, ecx
    je for2_end
    
    ; vnitrek
    ; if (pA[i] > pA[j]) { tmp = pA[i]; pA[i] = pA[j]; pA[j] = tmp; }
    mov ax, [esi+ebx*2] ;pA[i] 
    cmp ax, [esi+edx*2]
    jng dont_change
    
    ; vymen
    xchg ax, [esi+edx*2]
    mov [esi+ebx*2], ax
    
   dont_change:
  
    inc edx
    jmp for2
   for2_end:
    
    inc ebx
    jmp for1
  for1_end:
    
    
    pop ebx
    pop edx
    
    mov esp, ebp
    pop ebp
    ret
