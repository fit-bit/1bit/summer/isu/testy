%include "rw32-2018.inc"


extern strtoul
section .data    
        string db "300",0
       
section .text
CMAIN:
    push ebp
    mov ebp,esp
    
    mov esi,string
    mov ebx,5    
    call task31
    
    push __float32__(16.0)
    push __float32__(4.0)    
    call task32
    add esp,8

    mov esp,ebp
    pop ebp
    ret    
;
;--- Úkol 1 --- 
;
; Naprogramujte funkci, která převede řetězec na číselnou hodnotu. Registr ESI ukazuje 
; na řetězec, který reprezentuje 32bitové celé číslo bez znaménka v číselné soustavě  
; dané registrem EBX. Výsledné číslo uložte do registru EAX. 
;
; K řešení tohoto problému můžete (nemusíte) využít funkci jazyka C:
;
;   unsigned long int strtoul(const char *str, char **endptr, int base),
;
; která vrací 32bitovou hodnotu bez znaménka získanou převodem textové 
; reprezentace čísla v soustavě "base" uloženého na adrese "str".
; Ukazatel "endptr" není pro naše účely potřeba a lze ho nastavit na 0. 
;
; Ukázka návrhu řešení v jazyce C:
;
;   EAX = strtoul(ESI, 0, EBX);
;
; Vstup:
;   EBX = základ číselné soustavy, ze které se budou čísla převádět
;   ESI = ukazatel na řetězec reprezentující číslo 
;
; Výstup:
;   EAX = číslo převedené z textové reprezentace
;
; Důležité:
;   Funkce musí zachovat obsah všech registrů kromě EAX a příznakového registru.
;   Zásobníkový rámec není potřeba.
;   Funkce "strtoul" mění obsah některých registrů (např. ECX, EDX ...).
;
; Hodnocení: až 1 bod.
; 
task31:
    push ebx
    push ecx
    push edx
    push esi
    push edi
    
    push ebx
    push 0
    push esi
    call strtoul
    add esp, 12
    
    pop edi
    pop esi
    pop edx
    pop ecx
    pop ebx
    ret
;
;--- Úkol 2 ---
;
; Naprogramujte funkci, která vypočítá výraz f(x,y) uvedený níže. Funkci jsou předávány parametry x a y
; na zásobníku v pořadí od posledního k prvnímu (zprava doleva), parametry ze zásobníku uklízí volající 
; a výsledek funkce se vrací v FPU v registru ST0 (konvence jazyka C).
;
;
;           A      (x + y/4.125)*38 + sin(4 - 2*pi*x)
; f(x,y) = --- = -------------------------------------------
;           B              x - sqrt(abs(y))
;
;
; float task32(float x, float y)
;
; Vstup:
;   x: 32bitové číslo v plovoucí řádové čárce ve formátu IEEE754
;   y: 32bitové číslo v plovoucí řádové čárce ve formátu IEEE754
;
; Výstup:
;   ST0 = +Inf <=> A >= 0 && B == 0 (tedy čitatel >= 0 a jmenovatel == 0)
;   ST0 = -Inf <=> A < 0 && B == 0 (tedy čitatel <  0 a jmenovatel == 0)
;   ST0 = f(x,y) v ostatních případech
;
; Důležité:
;   Ve funkci NEsmíte použít instrukci FINIT (při odevzdání bude tato instrukce
;     z kódu odstraněna), předpokládejte, že registry koprocesoru jsou prázdné.
;   Ve funkci NEsmíte použít pro svá data datový segment (zásobník použít lze).
;   Funkce musí zachovat obsah všech registrů kromě registru EAX a příznakového registru.
;   Funkce musí vyprázdnit všechny registry FPU kromě ST0, kde bude výsledek funkce.
;   Konstantu lze vygenerovat direktivou __float32__(x) například takto: MOV EAX,__float32__(-4.45).
;   Konstantu lze do ST0 nahrát přes zásobník pomocí direktivy __float32__(x) takto: 
;       PUSH __float32__(10.5)
;       FLD dword [ESP]
;       ADD ESP,4
;
; Hodnocení: až 5 bodů.
;
task32:
    push ebp
    mov ebp, esp
    
    ; ebp+8 = x
    ; ebp+12 = y
    
    fld dword [ebp+8]; x
    fld dword [ebp+12]; y x
    PUSH __float32__(4.125)
    FLD dword [ESP]; 4.125 y x
    ADD ESP,4
    fdiv ; y/4.125 x
    fadd ; x+y/4.125
    PUSH __float32__(38.0)
    FLD dword [ESP]; 38 x+y/4.125
    ADD ESP,4
    fmul ; (x+y/4.125)*38
     PUSH __float32__(4.0)
     FLD dword [ESP]; 4 (x+y/4.125)*38
     ADD ESP,4
     PUSH __float32__(2.0)
     FLD dword [ESP]; 2 4 (x+y/4.125)*38
     ADD ESP,4
    fldpi ;pi 2 4 (x+y/4.125)*38
    fld dword [ebp+8];x pi 2 4 (x+y/4.125)*38
    fmul ;pi*x 2 4 (x+y/4.125)*38
    fmul ; 2*pi*x 4 (x+y/4.125)*38
    fsub ; 4-2*pi*x (x+y/4.125)*38
    fsin ; sin(4-2*pi*x) (x+y/4.125)*38
    fadd ; (x+y/4.125)*38+sin(4-2*pi*x)
    fld dword [ebp+8]; x A
    fld dword [ebp+12] ; y x A
    fabs ; abs(y) x A
    fsqrt ; sqrt(abs(y)) x A
    fsub ; x-sqrt(abs(y)) A
    ; B A
    
    ; kontrola b = 0
    fldz ; 0 B A
    fcomip; B A
    jne good
    
    ; kontrola jestli a je + nebo -
    fxch ; A B
    fldz ; 0 A B
    fxch ; A 0 B
    fcomi
    jb negative_inf ; A < 0
   
    fcomp
    fcomp
    fcomp ; vynuluje fpu
    PUSH 0x7f800000
    FLD dword [ESP]
    ADD ESP,4
    
    jmp end
   negative_inf:
    fcomp
    fcomp
    fcomp ; vynuluje fpu
    PUSH 0xff800000
    FLD dword [ESP]
    ADD ESP,4
    
    jmp end
   good:
    
    fdiv ; A/B
    
   end:
    fnclex
    mov esp, ebp
    pop ebp
    ret
